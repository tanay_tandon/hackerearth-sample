package com.hackerearth.hackerearthupcomingevents.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.hackerearth.hackerearthupcomingevents.config.constants.Key;

/**
 * Created by tanaytandon on 20/01/16.
 */
public class PrefUtils {

    private static SharedPreferences mSharedPreferences;
    private static SharedPreferences.Editor mEditor;

    public static void init(Context context) {
        mSharedPreferences = context.getSharedPreferences(Key.PREF_NAME, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
        mEditor.commit();
    }

    public static void setSyncAdapterSet(boolean value) {
        mEditor.putBoolean(Key.SYNC_ADAPTER_ENABLED, value).commit();
    }

    public static boolean isSyncAdapterSet() {
        return mSharedPreferences.getBoolean(Key.SYNC_ADAPTER_ENABLED, false);
    }
}
