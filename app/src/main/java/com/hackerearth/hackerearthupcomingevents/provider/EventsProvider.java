package com.hackerearth.hackerearthupcomingevents.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.hackerearth.hackerearthupcomingevents.config.constants.database.EventsContract;

public class EventsProvider extends ContentProvider {
    public static final String AUTHORITY = "com.hackearth.hackerearthupcomingevents.provider.Events";
    private static final int EVENTS_CODE = 1;

    private static final UriMatcher uriMatcher;

    private static final String DATABASE_NAME = "upcoming_events_db";
    private static final String EVENTS_TABLE_NAME = "events";
    private static final int DATABASE_VERSION = 1;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, "events", EVENTS_CODE);
    }

    private DatabaseHelper databaseHelper;

    private static final String CREATE_EVENT_TABLE = "CREATE TABLE " + EVENTS_TABLE_NAME +
            " (" + EventsContract.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            EventsContract.TITLE + " TEXT, " + EventsContract.URL + " TEXT," +
            EventsContract.THUMBNAIL + " TEXT, " + EventsContract.COVER_IMAGE + " TEXT, " +
            EventsContract.CHALLENGE_TYPE + " TEXT, " + EventsContract.COLLEGE + " TEXT, " +
            EventsContract.DATE + " TEXT, " + EventsContract.TIME + " TEXT, " +
            EventsContract.END_DATE + " TEXT, " + EventsContract.END_TIME + " TEXT, " +
            EventsContract.DESCRIPTION + " TEXT, " + EventsContract.IS_HACKEREARTH + " INTEGER, " +
            EventsContract.STATUS + " TEXT);";

    private static class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_EVENT_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " +  EVENTS_TABLE_NAME);
            onCreate(db);
        }
    }

    @Override
    public boolean onCreate() {
        this.databaseHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if(uriMatcher.match(uri) != EVENTS_CODE) {
            throw new IllegalArgumentException("Illegal uri for query : " + uri);
        }
        SQLiteDatabase database = this.databaseHelper.getReadableDatabase();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        switch (uriMatcher.match(uri)) {
            case EVENTS_CODE: {
                builder.setTables(EVENTS_TABLE_NAME);
                break;
            }
        }
        Cursor cursor = builder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {

        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if(uriMatcher.match(uri) != EVENTS_CODE) {
            throw new IllegalArgumentException("Illegal uri for query : " + uri);
        }
        SQLiteDatabase database = this.databaseHelper.getWritableDatabase();
        long id = database.insert(EVENTS_TABLE_NAME, null, values);
        if(id > 0) {
            Uri itemUri = ContentUris.withAppendedId(uri, id);
            return itemUri;
        }
        throw new SQLException("something went horribily wrong " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if(uriMatcher.match(uri) != EVENTS_CODE) {
            throw new IllegalArgumentException("Illegal uri for query : " + uri);
        }
        SQLiteDatabase database = this.databaseHelper.getWritableDatabase();
        int deleteCount = database.delete(EVENTS_TABLE_NAME, selection, selectionArgs);
        return deleteCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
