package com.hackerearth.hackerearthupcomingevents.api.service;

import android.util.Log;

import com.hackerearth.hackerearthupcomingevents.config.constants.Url;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

/**
 * Created by tanaytandon on 18/01/16.
 */
public class APIService extends RetrofitGsonSpiceService {

    public static final String TAG = APIService.class.getName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    @Override
    protected String getServerUrl() {
        return Url.BASE_URL;
    }
}
