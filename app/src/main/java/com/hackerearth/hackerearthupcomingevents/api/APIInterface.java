package com.hackerearth.hackerearthupcomingevents.api;

import com.hackerearth.hackerearthupcomingevents.config.constants.Url;
import com.hackerearth.hackerearthupcomingevents.model.LatestEventResponse;

import retrofit.http.GET;
import retrofit.http.Headers;

/**
 * Created by tanaytandon on 18/01/16.
 */
public interface APIInterface {

    @Headers({
            "Content-Type: application/json"
    })
    @GET(Url.EVENT_UPCOMING)
    public LatestEventResponse getLatestEvents();
}
