package com.hackerearth.hackerearthupcomingevents.api.service;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.hackerearth.hackerearthupcomingevents.R;
import com.hackerearth.hackerearthupcomingevents.api.request.LatestEventRequest;
import com.hackerearth.hackerearthupcomingevents.config.constants.database.EventsContract;
import com.hackerearth.hackerearthupcomingevents.model.Event;
import com.hackerearth.hackerearthupcomingevents.model.LatestEventResponse;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by tanaytandon on 19/01/16.
 */
public class EventsService extends Service {

    public static final String TAG = EventsService.class.getName();
    private SpiceManager mSpiceManager = new SpiceManager(APIService.class);

    @Override
    public void onCreate() {
        super.onCreate();
        mSpiceManager.start(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        LatestEventRequest latestEventRequest = new LatestEventRequest();

        mSpiceManager.execute(latestEventRequest, new RequestListener<LatestEventResponse>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {

            }

            @Override
            public void onRequestSuccess(LatestEventResponse latestEventResponse) {
                ContentValues[] contentValuesArray = new ContentValues[latestEventResponse.response.size()];
                int position = 0;
                for (Event event : latestEventResponse.response) {
                    contentValuesArray[position] = new ContentValues();
                    contentValuesArray[position].put(EventsContract.TITLE, event.title);
                    contentValuesArray[position].put(EventsContract.DESCRIPTION, event.description);
                    contentValuesArray[position].put(EventsContract.CHALLENGE_TYPE, event.challengeType);
                    contentValuesArray[position].put(EventsContract.COLLEGE, event.college);
                    contentValuesArray[position].put(EventsContract.COVER_IMAGE, event.coverImage);
                    contentValuesArray[position].put(EventsContract.DATE, event.date);
                    contentValuesArray[position].put(EventsContract.TIME, event.time);
                    contentValuesArray[position].put(EventsContract.END_DATE, event.endDate);
                    contentValuesArray[position].put(EventsContract.END_TIME, event.endTime);
                    contentValuesArray[position].put(EventsContract.STATUS, event.status);
                    contentValuesArray[position].put(EventsContract.THUMBNAIL, event.thumbnail);
                    contentValuesArray[position].put(EventsContract.URL, event.url);
                    contentValuesArray[position].put(EventsContract.IS_HACKEREARTH, event.isHackerearth);
                    position++;
                }
                UpdateEventsTask updateEventsTask = new UpdateEventsTask();
                updateEventsTask.setServiceStartId(startId);
                updateEventsTask.execute(contentValuesArray);
            }
        });
        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class UpdateEventsTask extends AsyncTask<ContentValues[], Void, Void> {

        private int startId;

        private void setServiceStartId(int startId) {
            this.startId = startId;
        }

        @Override
        protected Void doInBackground(ContentValues[]... params) {
            ContentResolver contentResolver = EventsService.this.getContentResolver();
            contentResolver.delete(EventsContract.CONTENT_URI, null, null);
            contentResolver.bulkInsert(EventsContract.CONTENT_URI, params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Intent intent = new Intent(getResources().getString(R.string.intent_filter_sync_completed));
            LocalBroadcastManager.getInstance(EventsService.this).sendBroadcast(intent);
            EventsService.this.stopSelf(startId);
        }
    }

}
