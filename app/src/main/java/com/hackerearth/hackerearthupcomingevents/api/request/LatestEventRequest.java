package com.hackerearth.hackerearthupcomingevents.api.request;

import com.hackerearth.hackerearthupcomingevents.api.APIInterface;
import com.hackerearth.hackerearthupcomingevents.model.LatestEventResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by tanaytandon on 18/01/16.
 */
public class LatestEventRequest extends RetrofitSpiceRequest<LatestEventResponse, APIInterface> {

    public LatestEventRequest() {
        super(LatestEventResponse.class, APIInterface.class);
    }

    @Override
    public LatestEventResponse loadDataFromNetwork() throws Exception {
        return getService().getLatestEvents();
    }
}
