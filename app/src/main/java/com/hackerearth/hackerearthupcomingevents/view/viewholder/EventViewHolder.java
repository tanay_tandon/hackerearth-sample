package com.hackerearth.hackerearthupcomingevents.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hackerearth.hackerearthupcomingevents.R;

/**
 * Created by tanaytandon on 20/01/16.
 */
public class EventViewHolder extends RecyclerView.ViewHolder {

    public ImageView ivEventImage;
    public TextView tvEventName;
    public TextView tvEventDescription;
    public TextView tvEventOngoing;
    public TextView tvEventStartTime;
    public TextView tvEventEndTime;
    public Button btnViewEventOnWeb;

    public EventViewHolder(View view, View.OnClickListener btnClickListener) {
        super(view);
        ivEventImage = (ImageView) view.findViewById(R.id.iv_event_image);
        tvEventName = (TextView) view.findViewById(R.id.tv_event_name);
        tvEventDescription = (TextView) view.findViewById(R.id.tv_event_description);
        tvEventOngoing = (TextView) view.findViewById(R.id.tv_event_on_going);
        tvEventStartTime = (TextView) view.findViewById(R.id.tv_event_start_time);
        tvEventEndTime = (TextView) view.findViewById(R.id.tv_event_end_time);
        btnViewEventOnWeb = (Button) view.findViewById(R.id.btn_view_on_web);
        btnViewEventOnWeb.setOnClickListener(btnClickListener);
    }
}
