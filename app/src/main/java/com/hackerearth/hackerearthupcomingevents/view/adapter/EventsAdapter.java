package com.hackerearth.hackerearthupcomingevents.view.adapter;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hackerearth.hackerearthupcomingevents.R;
import com.hackerearth.hackerearthupcomingevents.config.Router;
import com.hackerearth.hackerearthupcomingevents.config.constants.Values;
import com.hackerearth.hackerearthupcomingevents.model.Event;
import com.hackerearth.hackerearthupcomingevents.util.DeviceUtils;
import com.hackerearth.hackerearthupcomingevents.view.viewholder.EventViewHolder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class EventsAdapter extends RecyclerView.Adapter<EventViewHolder> implements View.OnClickListener {

    private Context mContext;
    private ArrayList<Event> events;
    private LayoutInflater layoutInflater;
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;

    public EventsAdapter(Context context, ArrayList<Event> events) {
        this.mContext = context;
        this.events = events;
        this.layoutInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.imageLoader = ImageLoader.getInstance();
        this.displayImageOptions = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .showImageOnLoading(R.drawable.ic_launcher).build();
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventViewHolder(layoutInflater.inflate(R.layout.item_event, parent, false), this);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        Event event = events.get(position);
        imageLoader.displayImage(event.coverImage, holder.ivEventImage, displayImageOptions);
        holder.tvEventName.setText(event.title);
        holder.tvEventDescription.setText(event.description);
        if(event.status.equalsIgnoreCase(Values.ONGOING)) {
            holder.tvEventOngoing.setVisibility(View.VISIBLE);
            holder.tvEventOngoing.setText(event.status);
            holder.tvEventStartTime.setVisibility(View.GONE);
        } else {
            holder.tvEventOngoing.setVisibility(View.GONE);
            holder.tvEventStartTime.setVisibility(View.VISIBLE);
            holder.tvEventStartTime.setText(Values.STARTS_AT + " " + event.date + " " + event.time);
        }
        holder.tvEventEndTime.setText(Values.ENDS_AT + " " + event.endDate + " " + event.endTime);
        holder.btnViewEventOnWeb.setTag(position);
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_view_on_web: {
                if(DeviceUtils.isNetworkConnected(mContext)) {
                    int position = (int) v.getTag();
                    Router.viewEventInBrowser(mContext, events.get(position).url);
                } else {
                    Snackbar.make(v, "You must be connected to the internet.", Snackbar.LENGTH_LONG).setAction("Open Settings", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Router.showSettingsActivity(mContext);
                        }
                    }).show();
                }
                    break;
            }

        }
    }
}
