package com.hackerearth.hackerearthupcomingevents.view.activity;

import android.accounts.Account;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.hackerearth.hackerearthupcomingevents.R;
import com.hackerearth.hackerearthupcomingevents.config.Router;
import com.hackerearth.hackerearthupcomingevents.config.constants.database.EventsContract;
import com.hackerearth.hackerearthupcomingevents.model.Event;
import com.hackerearth.hackerearthupcomingevents.provider.EventsProvider;
import com.hackerearth.hackerearthupcomingevents.sync.account.AccountUtils;
import com.hackerearth.hackerearthupcomingevents.util.DeviceUtils;
import com.hackerearth.hackerearthupcomingevents.util.PrefUtils;
import com.hackerearth.hackerearthupcomingevents.view.adapter.EventsAdapter;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    public static final long SECONDS_PER_MINUTE = 60L;
    public static final long SYNC_INTERVAL_IN_MINUTES = 60L;
    public static final long SYNC_INTERVAL = 24 * SYNC_INTERVAL_IN_MINUTES * SECONDS_PER_MINUTE;

    public static final String TAG = HomeActivity.class.getName();
    private EventsAdapter mEventsAdapter;
    private ArrayList<Event> events;
    private RecyclerView rvEvents;
    private View pbEventsLoading;
    private View rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        rvEvents = (RecyclerView) findViewById(R.id.rv_events);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        rvEvents.setLayoutManager(mLinearLayoutManager);
        rvEvents.setHasFixedSize(true);

        pbEventsLoading = findViewById(R.id.pb_loading_events);
        rootView = findViewById(R.id.cl_home_activity_root);

        pbEventsLoading.setVisibility(View.VISIBLE);

        events = new ArrayList<>();
        mEventsAdapter = new EventsAdapter(this, events);

        rvEvents.setAdapter(mEventsAdapter);

        if (PrefUtils.isSyncAdapterSet()) {
            new LoadEventsTask().execute();
        } else {
            if (DeviceUtils.isNetworkConnected(this)) {
                initializeSyncAdapter();
            } else {
                getLongSnackBarWithMessage("You must be connected to the internet").setAction("Open Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Router.showSettingsActivity(HomeActivity.this);
                    }
                }).show();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(brSyncCompleted, new IntentFilter(getResources().getString(R.string.intent_filter_sync_completed)));
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!PrefUtils.isSyncAdapterSet()) {
            if(DeviceUtils.isNetworkConnected(this)) {
                initializeSyncAdapter();
            } else {
                getLongSnackBarWithMessage("You must be connected to the internet").setAction("Open Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Router.showSettingsActivity(HomeActivity.this);
                    }
                }).show();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(brSyncCompleted);
    }

    private void initializeSyncAdapter() {
        showSnackBarMessage("Initializing the application please wait");
        Account account = AccountUtils.addAccount(this, getResources().getString(R.string.default_account), getResources().getString(R.string.authenticator_account_type));
        ContentResolver.setSyncAutomatically(account, EventsProvider.AUTHORITY, true);
        ContentResolver.requestSync(account, EventsProvider.AUTHORITY, Bundle.EMPTY);
        ContentResolver.addPeriodicSync(account, EventsProvider.AUTHORITY, Bundle.EMPTY, SYNC_INTERVAL);
    }

    private void showSnackBarMessage(String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show();
    }

    private Snackbar getLongSnackBarWithMessage(String message) {
        return Snackbar.make(rootView, message, Snackbar.LENGTH_LONG);
    }

    private BroadcastReceiver brSyncCompleted = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            new LoadEventsTask().execute();
        }
    };

    private class LoadEventsTask extends AsyncTask<Void, Void, ArrayList<Event>> {

        @Override
        protected void onPreExecute() {
            showSnackBarMessage("Loading Events");
            pbEventsLoading.setVisibility(View.VISIBLE);

        }

        @Override
        protected ArrayList<Event> doInBackground(Void... params) {
            ContentResolver contentResolver = HomeActivity.this.getContentResolver();
            Cursor cursor = contentResolver.query(EventsContract.CONTENT_URI, null, null, null, EventsContract.STATUS + " ASC");
            ArrayList<Event> events = new ArrayList<>();
            if (cursor != null) {
                cursor.moveToFirst();
                Log.d(TAG, cursor.getCount() + " ");
                while (cursor.moveToNext()) {
                    Event event = new Event();
                    event.title = cursor.getString(cursor.getColumnIndex(EventsContract.TITLE));
                    event.description = cursor.getString(cursor.getColumnIndex(EventsContract.DESCRIPTION));
                    event.challengeType = cursor.getString(cursor.getColumnIndex(EventsContract.CHALLENGE_TYPE));
                    event.college = cursor.getInt(cursor.getColumnIndex(EventsContract.COLLEGE)) == 1;
                    event.coverImage = cursor.getString(cursor.getColumnIndex(EventsContract.COVER_IMAGE));
                    event.date = cursor.getString(cursor.getColumnIndex(EventsContract.DATE));
                    event.time = cursor.getString(cursor.getColumnIndex(EventsContract.TIME));
                    event.endDate = cursor.getString(cursor.getColumnIndex(EventsContract.END_DATE));
                    event.endTime = cursor.getString(cursor.getColumnIndex(EventsContract.END_TIME));
                    event.url = cursor.getString(cursor.getColumnIndex(EventsContract.URL));
                    event.thumbnail = cursor.getString(cursor.getColumnIndex(EventsContract.THUMBNAIL));
                    event.challengeType = cursor.getString(cursor.getColumnIndex(EventsContract.CHALLENGE_TYPE));
                    event.status = cursor.getString(cursor.getColumnIndex(EventsContract.STATUS));
                    events.add(event);
                }
                cursor.close();
                return events;
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Event> events) {
            showSnackBarMessage("Loading events finished");
            pbEventsLoading.setVisibility(View.GONE);
            HomeActivity.this.events.clear();
            HomeActivity.this.events.addAll(events);
            HomeActivity.this.mEventsAdapter.notifyDataSetChanged();
            PrefUtils.setSyncAdapterSet(true);
        }
    }
}
