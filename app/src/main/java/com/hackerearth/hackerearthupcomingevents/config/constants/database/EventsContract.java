package com.hackerearth.hackerearthupcomingevents.config.constants.database;

import android.net.Uri;

import com.hackerearth.hackerearthupcomingevents.provider.EventsProvider;

/**
 * Created by tanaytandon on 18/01/16.
 */
public final class EventsContract {

    private static final String PROVIDER_URL = "content://com.hackearth.hackerearthupcomingevents.provider.Events/events";
    public static final Uri CONTENT_URI = Uri.parse(PROVIDER_URL);


    public static final String ID = "_id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String URL = "url";
    public static final String STATUS = "status";
    public static final String COLLEGE = "college";
    public static final String DATE = "date";
    public static final String TIME = "time";
    public static final String END_DATE = "end_date";
    public static final String END_TIME = "end_time";
    public static final String START_TIMESTAMP = "start_timestamp";
    public static final String END_TIMESTAMP = "end_timestamp";
    public static final String START_TZ = "start_tz";
    public static final String END_TZ = "end_tz";
    public static final String COVER_IMAGE = "cover_image";
    public static final String THUMBNAIL = "thumbnail";
    public static final String IS_HACKEREARTH = "is_hackerearth";
    public static final String CHALLENGE_TYPE = "challenge_type";


}
