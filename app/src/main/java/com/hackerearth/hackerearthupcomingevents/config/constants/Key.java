package com.hackerearth.hackerearthupcomingevents.config.constants;

/**
 * Created by tanaytandon on 20/01/16.
 */
public class Key {

    public static final String PREF_NAME = "upcoming_events_prefs";

    public static final String SYNC_ADAPTER_ENABLED = "sync_adapter_enabled";
}
