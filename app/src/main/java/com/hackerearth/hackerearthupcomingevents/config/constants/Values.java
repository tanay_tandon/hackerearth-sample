package com.hackerearth.hackerearthupcomingevents.config.constants;

/**
 * Created by tanaytandon on 20/01/16.
 */
public class Values {

    public static final String UPCOMING = "upcoming";

    public static final String ONGOING = "ongoing";
    public static final String STARTS_AT = "Starts At";
    public static final String ENDS_AT = "Ends At";
}
