package com.hackerearth.hackerearthupcomingevents.config.constants;

/**
 * Created by tanaytandon on 18/01/16.
 */
public class Url {

    public static final String BASE_URL = " https://www.hackerearth.com/api/";
    public static final String EVENT_UPCOMING = "/events/upcoming/";
}
