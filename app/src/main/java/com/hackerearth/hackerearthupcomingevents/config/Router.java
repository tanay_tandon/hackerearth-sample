package com.hackerearth.hackerearthupcomingevents.config;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;

import com.hackerearth.hackerearthupcomingevents.api.service.EventsService;

/**
 * Created by tanaytandon on 18/01/16.
 */
public class Router {

    public static void startEventsService(Context context) {
        Intent intent = new Intent(context, EventsService.class);
        context.startService(intent);
    }

    public static void viewEventInBrowser(Context context, String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }

    public static void showSettingsActivity(Context context) {
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        context.startActivity(intent);
    }
}
