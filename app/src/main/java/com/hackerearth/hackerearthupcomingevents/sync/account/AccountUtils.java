package com.hackerearth.hackerearthupcomingevents.sync.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.util.Log;

/**
 * Created by tanaytandon on 19/01/16.
 */
public class AccountUtils {
    public static Account addAccount(Context context, String accountName, String accountType) {
        Account account = new Account(accountName, accountType);
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        Log.d("AccountUtils", "" + accountManager.addAccountExplicitly(account, null, null));
        return account;
    }
}
