package com.hackerearth.hackerearthupcomingevents.sync.adapter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.hackerearth.hackerearthupcomingevents.config.HackerEarthApplication;

/**
 * Created by tanaytandon on 19/01/16.
 */
public class SyncService extends Service {
    private static SyncAdapter syncAdapter = null;

    private static final Object sSyncAdapterLock = new Object();

    private static final String TAG = SyncService.class.getName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "sync service created");
        synchronized (sSyncAdapterLock) {
            if (syncAdapter == null) {
                Log.d(TAG, "is context same" + (this.getApplicationContext() instanceof HackerEarthApplication));
                syncAdapter = new SyncAdapter(this.getApplicationContext(), true);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "binder returned");
        return syncAdapter.getSyncAdapterBinder();
    }
}
