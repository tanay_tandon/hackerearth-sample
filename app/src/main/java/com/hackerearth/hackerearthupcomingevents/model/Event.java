package com.hackerearth.hackerearthupcomingevents.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hackerearth.hackerearthupcomingevents.config.constants.database.EventsContract;

public class Event {
    @SerializedName(EventsContract.TITLE)
    @Expose
    public String title;
    @SerializedName(EventsContract.DESCRIPTION)
    @Expose
    public String description;
    @SerializedName(EventsContract.URL)
    @Expose
    public String url;
    @SerializedName(EventsContract.STATUS)
    @Expose
    public String status;
    @SerializedName(EventsContract.COLLEGE)
    @Expose
    public boolean college;
    @SerializedName(EventsContract.DATE)
    @Expose
    public String date;
    @SerializedName(EventsContract.TIME)
    @Expose
    public String time;
    @SerializedName(EventsContract.END_DATE)
    @Expose
    public String endDate;
    @SerializedName(EventsContract.END_TIME)
    @Expose
    public String endTime;
    @SerializedName(EventsContract.COVER_IMAGE)
    @Expose
    public String coverImage;
    @SerializedName(EventsContract.THUMBNAIL)
    @Expose
    public String thumbnail;
    @SerializedName(EventsContract.IS_HACKEREARTH)
    @Expose
    public boolean isHackerearth;
    @SerializedName(EventsContract.CHALLENGE_TYPE)
    @Expose
    public String challengeType;
}
