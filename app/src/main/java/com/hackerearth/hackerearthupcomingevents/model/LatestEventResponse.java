package com.hackerearth.hackerearthupcomingevents.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LatestEventResponse {

    @SerializedName("response")
    @Expose
    public List<Event> response = new ArrayList<>();
}
